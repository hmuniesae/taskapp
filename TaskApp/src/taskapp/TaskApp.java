package taskapp;

import java.util.Arrays;
import java.util.Scanner;

public class TaskApp {
	//Array de tareas
	static String[] tasks = {"Estudiar examen", "Practica entornos", "Encontrar trabajo"};
	//Entrada por consola
	static Scanner sc;
	
	public static void main(String[] args) {
		//Inicializar el escaner
		sc = new Scanner(System.in);
		
		int option = -1;
		
		//Ejecutar mientras no elijamos salir
		while(option != 0) {
			// Mostrar las tareas
			System.out.println("\nTAREAS\n------\n");
			if(tasks.length > 0) {
				for (int i = 0; i < tasks.length; i++) {
					System.out.println((i+1) + " - " +tasks[i]);
				}
			}else {
				System.out.println("No hay tareas pendientes");
			}
			//Mostrar opciones
			System.out.println("\nOpciones");
			System.out.println("1 - Tarea hecha");
			System.out.println("2 - Nueva tarea");
			System.out.println("0 - Salir");
			
			option = askInt("Selecciona una opcion");
			
			switch (option) {
			case 1:
				doTask();
				
				break;
			case 2:
				addTask();
				break;
			case 0:
				break;
			default:
				System.out.println("Opcion invalida");
				break;
			}
			
			
		}
		
		
		
		
		//Cerrar escaner
		sc.close();

	}
	
	private static void addTask() {
		//Preguntar la tarea que queremos añadir
		System.out.println("Introduce la descripcion de la nueva tarea");
		String newTask = sc.nextLine();
		
		//Crear un nuevo array con una posicion mas y copiar array viejo sobre el nuevo
		String[] newTasks = Arrays.copyOf(tasks, tasks.length + 1);
		
		//Añadir la nueva tarea
		newTasks[newTasks.length - 1] = newTask;
		
		//Asignar el nuevo array
		tasks = newTasks;
	}

	private static void doTask() {
		//Pedir tarea hecha
		int pos = askInt("Introducir el numero de la tarea hecha");
		pos--; //Ajustar al indice del array
		
		//Verificar posicon
		if(pos<0 || pos>=tasks.length) {
			System.out.println("El indice no esta en la lista");
			return;
		}
		
		//Crear un array con un elemento menos
		String[] newTasks= new String[tasks.length - 1];
		int j = 0; //Indice para el nuevo array
		
		//Recorrer array original y volcar todos los elementos menos el seleccionado
		for (int i = 0; i < tasks.length; i++) {
			if(i!=pos) {
				newTasks[j] = tasks[i];
				j++;
			}
		}
		
		//asignar el nuevo array a la variable tasks
		tasks = newTasks;
	}

	private static int askInt(String msg) {
		// pedimos numero y aseguramos que es int
		System.out.println(msg);
		while (!sc.hasNextInt()) { // aseguramos que hay un int
			sc.nextLine();
			System.out.println(msg);
		}
		// obtener numero del usuario
		int num = sc.nextInt();
		sc.nextLine();
		return num;
	}

}
